# -*- coding: utf-8 -*-
require 'rake'
require 'active_support/all'
require 'digest'

class AccountDataCSVImporter
  IMPORT_DIR = "."
  # IMPORT_DIR = "/home/cjk/family/banking"

  def self.run
    voba = VobaCSVParser.new
    @filelist = FileList.new("#{IMPORT_DIR}/*.csv")
    @filelist.each do |f|
      puts "Parsing file #{f}..."
      rows = voba.parse(f)
      postings = voba.to_postings(rows).apply_rules
      # puts postings
      postings.map {|post| AccountRulesEngine.build_booking_line(post)}
      LedgerEmitter.new(postings).emit
    end

    return true
  end
end

class VobaCSVParser
  require 'smarter_csv'

  attr_reader :postings

  # Converter class to convert parsed numbers, ... to strings
  class StringConverter
    def self.convert(value)
      value.to_s
    end
  end

  # We use this hash to compare if two rows are duplicates
  def hashFromRow(row)
    Digest::SHA256.hexdigest row[:booked] + row[:receiver] + row[:comment] + row[:amount]
  end

  HEADERS = %W(booked valuta sender receiver account_no iban bank_no bic comment cust_ref currency amount type)

  def parse(file)
    options = {:chunk_size => 100, :col_sep => ';', :row_sep => :auto, :headers_in_file => false, :user_provided_headers => HEADERS, :remove_empty_values => false, :value_converters => {:sender => StringConverter, :receiver => StringConverter, :account_no => StringConverter, :bank_no => StringConverter}}
    rows = []; dupeHashes = Array.new

    SmarterCSV.process(file, options) do |chunk|

      rows.concat(
        # Eliminate some duplicates, by which our current definition is creating
        # a hash-digest over booking-date, receiver, comment-text and amount.
        chunk.reject { |row|
          date, receiver, comment, amount = row[:booked], row[:receiver], row[:comment], row[:amount]
          hash = hashFromRow(row)

          isDupe = chunk.reject { |e| dupeHashes.any? { |dh| dh == hash } }.select { |row2|
            row2[:booked] == date && row2[:receiver] == receiver && row2[:comment] == comment && row2[:amount] == amount
          }.count() > 1
          if isDupe
            dupeHashes.push(hash)
            puts "WARNING: Found likely duplicate record: #{date}, #{receiver}, #{comment}, #{amount} - REMOVING."
          end
          isDupe
        })

    end

    rows
  end

  def to_postings(rows)
    @postings = []
    rows.collect {|row|
      tx = {
        :value => row[:amount].tr('.', '').tr(',', '.').to_f,
        :when => row[:booked].to_date.strftime('%Y/%m/%d'),
        :comment => row[:comment]
      }
      tx[:src]  = AccountRulesEngine.find_booking_category_for({:sender => row[:sender], :receiver => row[:receiver], :account_no => row[:account_no], :comment => row[:comment]})
      if row[:type] == 'S'
        tx[:type] = 'expense'
        tx[:from] = row['assets:checking']
        tx[:to]   = row[:receiver]
      else
        tx[:type] = 'income'
        tx[:from] = row[:receiver]
        tx[:to]   = row['assets:checking']
      end
      @postings << tx
    }
    self
  end

  def apply_rules
    @postings.select {|p| p[:src]}.map {|p| AccountRulesEngine.apply_rules(p[:src]); p}
  end

end

class AccountRulesEngine
  # NOTE: Booking-lines are matched against postings in _order_ so when two postings would both match, the first one wins!
  POSTINGS = [
    { name: 'nobody', :sig => {receiver: 'nobody'}}, # the fallback entry
    { name: 'konto_voba', :sig => {comment: 'ABSCHLUSS PER'}},
    { name: 'onlinebanking_voba', :sig => {receiver: 'ONLINEBANKING'}},
    { name: 'bellicon_deutschland', :sig => {receiver: 'bellicon deutschland'}},
    { name: 'bellicon_div', :sig => {receiver: 'Seminarinstitut proLife'}},
    { name: 'kreditkarte_lufthansa_1', :sig => {comment: '992009498164335|992001876681535'}}, # match Lufthansa card number
    { name: 'kreditkarte_lufthansa_2', :sig => {receiver: 'LUFTHANSA CARD'}},
    { name: 'kreditkarte_visa_voba', :sig => {receiver: 'volksbank', comment: '45703856|390000060709701900'}},
    { name: 'kreditkarte_barclaycard_platinum', :sig => {receiver: 'barclay', comment: 'DE3120130600|21351033543'}},
    { name: 'nzz', :sig => {receiver: 'NEUE (ZUERCHER|ZÜRCHER) ZEITUNG|NZZ Deutschland'}},
    { name: 'zeitungen_div', :sig => {receiver: 'Kreiszeitung'}},
    { name: 'bücher_1', :sig => {receiver: 'DER WALDENBUCHLADEN|OSIANDER|Buchhandlung Seiffert'}},
    { name: 'mit_kindern_wachsen_1', :sig => {account_no: '294167752'}},
    { name: 'mit_kindern_wachsen_2', :sig => {receiver: 'Arbor Verlag|PressUp\W+'}},
    { name: 'msc_gehalt', :sig => {receiver: 'MSC', comment: 'GEHALT'}},
    { name: 'ittem_gehalt', :sig => {receiver: 'IT\.TEM GmbH', comment: 'Gehalt'}},
    { name: 'activpersonal_gehalt', :sig => {receiver: 'ACTIV PERSONAL', comment: 'Gehalt'}},
    { name: 'einkommen_diebold', :sig => {comment: 'DE9464163225005050600'}},
    { name: 'einkommen_fischer_consulting', :sig => {comment: 'DE1060050101000580736'}},
    { name: 'einkommen_forum_der_kulturen', :sig => {comment: 'DE94600501010002203465'}},
    { name: 'einkommen_xitcom', :sig => {comment: '82604700240040091100'}},
    # NOTE: once we handle transactions for our Deutsche-Bank account separately, we have to remove this as it otherwise
    # causes double-bookings:
    { name: 'einkommen_via_deutsche_bank_konto', :sig => {comment: 'DE.*8.*6600700240032573805'}},
    { name: 'einkommen_via_deutsche_bank_konto_2', :sig => {receiver: 'claus.*klingberg', comment: 'Deutsche-Bank'}},
    { name: 'auto_steuern', :sig => {receiver: 'BUNDESKASSE[\s\w]*WEIDEN', comment: 'KFZ-STEUER'}},
    { name: 'auto_versicherung_1', :sig => {receiver: 'R\+V', comment: '466247456'}},
    { name: 'auto_versicherung_2', :sig => {comment: 'B\s*B\s*-Q\s*168'}},
    { name: 'auto_werkstatt_arnold', :sig => {receiver: 'AUTOHAUS ARNOLD'}},
    { name: 'auto_werkstatt_mueller', :sig => {receiver: 'MUELLER (KFZ|AUTOR)|rosemarie mueller'}},
    { name: 'auto_werkstatt_jaeger', :sig => {receiver: 'JAEGER FAHRZEUGTECHNIK'}},
    { name: 'auto_teile_div', :sig => {receiver: 'A[\.]?T[\.]?U|AUTO-TEILE-UNGER'}},
    { name: 'stadtmobil', :sig => {receiver: 'stadtmobil'}},
    { name: 'tilgung_kredit_var', :sig => {receiver: 'TEILZAHLUNG DARLEHEN', account_no: '607097205'}},
    { name: 'tilgung_kredit_var_2', :sig => {comment: 'tilgung.*607097205'}},
    { name: 'tilgung_kredit_haus', :sig => {receiver: '(TEILZAHLUNG|ANNUITÄT) DARLEHEN', account_no: '607097213'}},
    { name: 'tilgung_kredit_haus_commerzbank', :sig => {receiver: 'Commerzbank', comment: 'DE73508400050135307701.*DE3811000000020140|DE735084'}},
    { name: 'sondertilgung_kredit_haus', :sig => {sender: 'Klingberg', receiver: 'Klingberg', account_no: '607097213'}},
    { name: 'sondertilgung_kredit_haus_1', :sig => {sender: 'Klingberg', receiver: 'Klingberg', comment: '097213'}},
    { name: 'strom_enbw_1', :sig => {comment: 'STROM', account_no: '7402051588'}},
    { name: 'strom_enbw_2', :sig => {receiver: '^EnBW'}},
    { name: 'strom_stadtwerke_tue_1', :sig => {comment: 'Lastschrift', account_no: '168100'}},
    { name: 'strom_stadtwerke_tue_2', :sig => {account_no: '140199100'}},
    { name: 'strom_stadtwerke_tue_3', :sig => {receiver: 'STADTWERKE TUEBINGEN'}},
    { name: 'strom_knauber', :sig => {receiver: '^KNAUBER'}},
    { name: 'strom_stadtwerke_oranienburg', :sig => {receiver: 'stadtwerke oranien'}},
    { name: 'wartung_heizung', :sig => {receiver: 'Knapp Kälteservice'}},
    { name: 'wartung_heizung_lindheimer', :sig => {comment: 'DE426039'}},
    { name: 'verein_flötentöne_zirkus', :sig => {comment: 'Floetentoene', account_no: '654900000'}},
    { name: 'verein_tsv_waldebuch', :sig => {receiver: 'TSV WALDENBUCH'}},
    { name: 'verein_tsv_leinfelden', :sig => {receiver: 'TSV Leinfelden'}},
    { name: 'verein_chorverein_waldenbuch', :sig => {receiver: 'Chorverein Waldenbuch'}},
    { name: 'verein_ibm_club', :sig => {receiver: 'ibm klub'}},
    { name: 'verein_ewto', :sig => {receiver: 'WINGTSUN'}},
    { name: 'kabelbw_1', :sig => {account_no: '166262660'}},
    { name: 'kabelbw_2', :sig => {receiver: 'UNITYMEDIA|KABEL[ ]?BW'}},
    { name: 'telekom', :sig => {receiver: 'Telekom Deutsch|deutsche telekom'}},
    { name: 'internet_hoster', :sig => {receiver: 'contabo'}},
    { name: 'fas_schulgeld', :sig => {receiver: 'freie aktive schule', comment: 'Schulgeld'}},
    { name: 'fas_spenden', :sig => {receiver: 'freie aktive schule', comment: 'spende'}},
    { name: 'fas_rest', :sig => {receiver: 'freie aktive schule'}},
    { name: 'fas_schliessfächer', :sig => {receiver: 'AstraDirekt|AstraDirect'}}, # Miete Schliessfächer
    { name: 'fas_ausflüge', :sig => {receiver: 'anne[ ]+', comment: 'ausflug|essen|reinigung|familienwochenende'}},
    { name: 'fas_ausflüge_2', :sig => {receiver: 'jonas beisenwenger', comment: 'taunus'}},
    { name: 'fas_putzen_ehlers', :sig => {receiver: '[SABINE |TAMARA ]?EHLERS'}},
    { name: 'fas_putzen_div', :sig => {comment: 'Putzdienst.*(FAS|0260390000043)'}},
    { name: 'andere_voba_konten', :sig => {account_no: '607097604'}},
    { name: 'lbbw_konto', :sig => {account_no: '3066034'}},
    { name: 'n26_konto_ling', :sig => {receiver: '^ling', comment: 'NTSBDEB1'}},
    { name: 'sparkonto_nicole_voba', :sig => {receiver: 'nicole', comment: '81451801'}},
    { name: 'sparkonto_daniel_voba', :sig => {receiver: 'daniel', comment: '381450007'}},
    { name: 'sparkonto_emma_voba', :sig => {receiver: '^emma|Emma Klingberg', comment: '439166'}},
    { name: 'nicole_taschengeld_voba', :sig => {account_no: '381451402', comment: 'Rateneinzug'}},
    { name: 'nicole_taschengeld_voba_2', :sig => {comment: 'Nicole Klingberg.*Taschengeld|603900000381451003'}},
    { name: 'nicole_taschengeld_voba_3', :sig => {receiver: 'Nicole Klingberg', comment: '603900000381451003|taschengeld'}},
    { name: 'emma_kurs_gitarre', :sig => {receiver: 'Michael Scharr', comment: 'Emma'}},
    { name: 'eltern', :sig => {receiver: '(KLINGBERG.*[GERHARD|MARGARETHE]|Gerhard Klingberg)'}},
    { name: 'waki', :sig => {account_no: '643423001'}},
    { name: 'faulhaber_dl_v1', :sig => {account_no: '605878005'}},
    { name: 'faulhaber_dl_v2', :sig => {comment: '6600501010004352242|4603900000605878005'}},
    { name: 'faulhaber_dl_v3', :sig => {receiver: 'fritz faulhaber', sender: 'Klingberg'}},
    { name: 'faupel', :sig => {account_no: '2708824'}},
    { name: 'handwerker_1', :sig => {receiver: 'Silvio Mai'}},
    { name: 'gez_1', :sig => {account_no: '1362826'}},
    { name: 'gez_2', :sig => {comment: 'DE26600501010001362826'}},
    { name: 'gez_3', :sig => {receiver: '^Rundfunk'}},
    { name: 'congstar_handy', :sig => {receiver: 'congstar'}},
    { name: 'o2_handy1', :sig => {account_no: '5713153|225563601'}},
    { name: 'eins_und_eins_handy', :sig => {comment: 'K503975094'}},
    { name: 'klarmobil_handy', :sig => {receiver: 'klarmobil'}},
    { name: 'drillisch_handy', :sig => {receiver: 'drillisch.+gmbh'}},
    { name: 'aldi_talk_handy', :sig => {receiver: '^E-Plus'}},
    { name: 'tankstellen', :sig => {receiver: 'ARAL|ESSO|AGIP|TOTAL|AVIA|JET|OMV|SHELL|^TANKST|Eni Deutschland|Tamoil|AUTOHAUS SCHOPP'}},
    { name: 'tankstellen_2', :sig => {comment: 'BP TANKSTELLE'}},
    { name: 'wasser', :sig => {receiver: 'WALDENBUCH|Stadt Waldenbuch', comment: 'WASSER'}},
    { name: 'abfall_betrieb_bb', :sig => {receiver: 'ABFALLWIRTSCHAFTSBETRIEB'}},
    { name: 'abfall_betrieb_bb_2', :sig => {comment: 'DE94GVA00'}},
    { name: 'lebensmittel_läden', :sig => {receiver: 'BIO|ERDI|DENNS|NATURKOST|ALNATURA|PENNY|EDEKA|ALDI (SUED|GMBH)|^NETTO|NATURGUT|LIDL|HACKER CLAUDIA|BENZ GETRAENKE|KAUFLAND|^rewe'}},
    { name: 'lebensmittel_1', :sig => {comment: 'EDEKA\s+|ALTIN KASAP'}},
    { name: 'lebensmittel_2', :sig => {receiver: 'SCHÖNHAAR|Schoenhaar'}},
    { name: 'lebensmittel_3', :sig => {receiver: 'BAIERSBACHHOF|ALBER WALTER'}},
    { name: 'lebensmittel_4', :sig => {receiver: 'METZGEREI|alfred ritter|^sehne|^K+U'}},
    { name: 'lebensmittel_5', :sig => {receiver: 'markus buban', comment: 'bestellung'}},
    { name: 'lebensmittel_div', :sig => {receiver: 'mopeti|stadtmuehle|markus buban|cash and carry|orient master|RUDOLF GMELIN|biomammut'}},
    { name: 'restaurants_1', :sig => {receiver: '^subway|restaurant|SALE E PEPE|PHO SAIGON'}},
    { name: 'restaurants_2', :sig => {comment: 'restaurant|neckarmueller|SAMPHAT-THAI|SUBWAY\s+'}},
    { name: 'gaertnereien', :sig => {receiver: 'FLORA GARTEN|GAERTNEREI|PFLANZENGAERTNEREI'}},
    { name: 'drogerie_dm_1', :sig => {receiver: 'DROGERIEMARKT'}},
    { name: 'drogerie_dm_2', :sig => {account_no: '7402099997'}},
    { name: 'drogerie_schlecker', :sig => {receiver: 'SCHLECKER'}},
    { name: 'paypal_1', :sig => {account_no: '175526300'}},
    { name: 'paypal_2', :sig => {receiver: '^PayPal|^Klarna'}},
    { name: 'kindergeld', :sig => {receiver: 'Bundesagentur fuer Arbeit|FAMILIENKASSE'}},
    { name: 'elterngeld', :sig => {comment: 'BEEG 335490'}},
    { name: 'ec_automat_1', :sig => {sender: 'EC-AUTOMAT'}},
    { name: 'ec_automat_2', :sig => {receiver: 'GA NR|VOLKSBANK MÜNSINGEN|VB FILDER|VR BANK TÜBINGEN|VB KIRCHHEIM'}},
    { name: 'ec_automat_3', :sig => {receiver: 'EC-GA'}},
    { name: 'ec_automat_4', :sig => {receiver: 'EC-POS EMV'}}, # Ausland
    { name: 'ec_automat_5', :sig => {comment: 'GA 60390000|GA 60090100|GA 60060396|GA 60050101|GA 60062775'}},
    { name: 'ec_automat_voba_fellbach', :sig => {receiver: 'FELLBACHER BANK'}},
    { name: 'barauszahlung_giro', :sig => {comment: 'Barauszahlung|Bargeldauszahlung|SB-Auszahlung'}},
    { name: 'depotkosten_deutsche_bank', :sig => {comment: 'EIS24003281380028012019'}},
    { name: 'carsharing_seligs', :sig => {receiver: 'SELIG', comment: 'CARSHARING'}},
    # { name: 'debeka_vers_beiträge', :sig => {receiver: 'DEBEKA VERSICHERUNGEN'}},
    { name: 'debeka_haftpflicht_claus_1', :sig => {receiver: 'DEBEKA VERSICHERUNGEN', comment: '5970474'}},
    { name: 'debeka_haftpflicht_claus_2', :sig => {receiver: 'Debeka Kranken-Versicherung', comment: 'HAFTPFL'}},
    { name: 'debeka_rente_ling_1', :sig => {receiver: 'DEBEKA VERSICHERUNGEN', comment: '5944002'}},
    { name: 'debeka_rente_ling_2', :sig => {receiver: 'Debeka.*(kranken|leben).*versicherung', comment: 'MALADE51KO'}},
    { name: 'union_krankenversicherung_kinder', :sig => {receiver: 'UNION KRANKENVERSICHERUNG', comment: 'Chen'}},
    { name: 'union_krankenversicherung_kinder_erstattungen', :sig => {comment: 'Scheckeinreichung'}},
    { name: 'hansemerkur_vers_beiträge_1', :sig => {receiver: 'HANSEMERKUR'}}, # Krankenversicherung
    { name: 'hansemerkur_vers_beiträge_2', :sig => {receiver: 'HANSEMERKUR', comment: 'Krankenversicherung'}}, # Krankenversicherung
    { name: 'sparkassen_vers_beiträge_1', :sig => {receiver: 'SV SPARKASSEN VERSICHERUNG'}}, # Berufunfähigkeitsversicherung
    { name: 'sparkassen_vers_beiträge_2', :sig => {receiver: 'SV Lebensversicherung', comment: '00280535980'}}, # Berufunfähigkeitsversicherung
    { name: 'techniker_krankenkasse_vers_beiträge', :sig => {receiver: 'TECHNIKER KRANKENKASSE'}}, # freiw. Krankenversicherung Claus
    { name: 'r_und_v_versicherung', :sig => {receiver: 'r[\W]?\+[\W]?v'}}, # Claus R+V Lebensversicherung / Rentenversicherung
    { name: 'aachen_muenchener_claus_basis_und_riester_rente', :sig => {receiver: 'AachenMuenchener Lebensversicherung AG', comment: '00MREF000000019'}}, # Claus Aachen Münchener Rentenversicherungen
    { name: 'generali_claus_basis_und_riester_rente', :sig => {receiver: 'generali', comment: '6870409'}}, # ab 2020: Claus Generali Rentenversicherungen
    { name: 'reiseversicherungen', :sig => {receiver: '^ERV|reiseversicherung'}},
    { name: 'neue_bayrische_leben', :sig => {receiver: 'Bayerische', comment: 'B090390194864'}}, # Ling Lebensversicherung / Rentenversicherung ab 2016
    { name: 'finanzamt_stuttgart_umsatzsteuer', :sig => {comment: '97054\/00342.*umsatz|umsatz.*97054.*00342|umsatz.*MARKDEF1600'}},
    { name: 'finanzamt_stuttgart', :sig => {receiver: 'FINANZAMT STUTTGART|^Finanzamt Stuttgart|Finanzkasse Stuttgart'}},
    { name: 'steuersoftware_buhldata', :sig => {receiver: 'BUHL DATA SERVICE'}},
    { name: 'waldenbuch_grundsteuer', :sig => {receiver: 'STADT WALDENBUCH', comment: 'GRUNDSTEUER'}},
    { name: 'optiker', :sig => {receiver: 'OPTIK BERKMANN|BERKMANN OPTIK|BRILLEN FELIX'}},
    { name: 'kleidung_diverse', :sig => {receiver: 'HUGO BOSS|WOICK|BRUNO MILLER|HENNES.*MAURITZ|H&M|H\+M|S OLIVER|BOESNER|JAKO-O|ESPRIT|MEXX|ENGEL GMBH|SCHIESSER AG|FALKE FOC|^HEM|MARC O POLO|Marc.*Polo|INTERSP|MCTREK|TAUSENDFUESSLER|BEZAHLBAR|SPORT BAUR|C&A|Traub Holger|decathlon|Schuhhaus Geiger|LEGUANO|^KMG|BLAUE ECK'}},
    { name: 'unterhaltung_elektro_div', :sig => {receiver: '^MEDIA MARKT'}},
    { name: 'schuhhäuser', :sig => {receiver: 'SCHUHHAUS BREUNING|SCHUHHAUS BUBSER|quick schuh|GEA TUEBINGEN'}},
    { name: 'schuhhaus_bubser', :sig => {account_no: '605375003'}},
    { name: 'schornsteinfeger_beetz', :sig => {receiver: 'ALF-DIETER BEETZ'}},
    { name: 'möbel_div', :sig => {receiver: 'IKEA|OPOCZYNSKI|KARL ROGG|MAIERS BETTWAREN|BETTENLAGER|holz waidelich|WAIDELICH SAEGEWERK|BUTLERS'}},
    { name: 'kaufhäuser', :sig => {receiver: '^BREUNINGER|^KAUFHOF'}},
    { name: 'fahrrad_div', :sig => {receiver: '^CYCLE'}},
    { name: 'adac', :sig => {receiver: 'ADAC'}},
    { name: 'amazon', :sig => {receiver: 'AMAZON'}},
    { name: 'nanu_nana', :sig => {receiver: 'NANU-NANA'}},
    { name: 'yu_mei_reisebüro', :sig => {receiver: 'YU-MEI'}},
    { name: 'div_reisebüros', :sig => {receiver: 'ABCTravel'}},
    { name: 'spiel_und_spass_div', :sig => {receiver: 'FILDORADO|hallenbad|sensapolis'}},
    { name: 'bücher_hörspiele', :sig => {receiver: 'audible'}},
    { name: 'ski_schullandheim', :sig => {receiver: 'Rolf Schreiber|HOBEX AG', comment: 'Ski|Bezau'}},
    { name: 'ski_schullandheim_ausrüstung', :sig => {comment: '(DORNER|ANDELSBUCH|SPORT THOMAS)'}},
    { name: 'baumärkte', :sig => {receiver: 'HORNBACH|^TOOM|BM FELLBACH|^OBI|BAUFACHMARKT AUCH|Dehner Gartencenter'}},
    { name: 'ärzte', :sig => {receiver: '^PRAXIS|^DR S|DR[\.]? MOESSNER|OSTEOPATHIE'}},
    { name: 'ärzte_2', :sig => {receiver: 'PRAXIS HEIKE KÖTZLE|Angela Gerdes|Ariane Conrad|^Dr.*Haller|Gerner-Ott'}},
    { name: 'tcm_ärzte', :sig => {comment: '0901000116180005|406005010100'}}, # IBANs Hr. Eppler, Fr. Wang
    { name: 'zahnärzte', :sig => {receiver: 'GOEBEL.*NEFF|ZAHNARZTPRAXIS|dr.*kleber|health ag|(dr|friedbert).*laible|zahnarzt.*dr'}},
    { name: 'zahnärzte_2', :sig => {comment: 'friedbert laible'}},
    { name: 'kieferorthopäden_lastschriften', :sig => {comment: 'DE40KFO0000|DE40KFO00000150'}},
    { name: 'kieferorthopäden_lastschriften_2', :sig => {account_no: '25870157'}},
    { name: 'apotheken', :sig => {receiver: '^APO |APOTHEKE|Europa Apotheek'}},
    { name: 'apotheken_2', :sig => {comment: '(RATS|HERZ)\W+APOTHEKE'}},
    { name: 'dr_muente', :sig => {receiver: 'DR[\.]? MUENTE|DR\. MÜNTE'}},
    { name: 'ct_magazin', :sig => {receiver: 'HEISE (ZEITSCHRIFTEN|MEDIEN)'}},
    { name: 'unerzogen', :sig => {receiver: 'tologo verlag'}},
    { name: 'disney_ltb', :sig => {receiver: 'abo-service walt disney'}},
    { name: 'magazin_geo_1', :sig => {comment: 'DPVDEUTSCHE'}},
    { name: 'magazin_geo_2', :sig => {receiver: '^DPV'}},
    { name: 'hunde_steuer', :sig => {comment: 'HUNDESTEUER'}},
    { name: 'hunde_tierarzt', :sig => {receiver: '^tierarzt'}},
    { name: 'hunde_haftpflicht_nv', :sig => {receiver: 'NV-Versicherungen'}},
    { name: 'hunde_futter', :sig => {receiver: 'hund.*katz'}},
    { name: 'hunde_futter_2', :sig => {receiver: 'Helga Fehrenbach'}},
    { name: 'hunde_pflege', :sig => {receiver: 'Jürgen Rentschler'}},
    { name: 'bahn_1', :sig => {receiver: 'DEUTSCHE BAHN'}},
    { name: 'ssb_1', :sig => {receiver: 'STUTTGARTER STRASSENBAHNEN'}},
    { name: 'ssb_2', :sig => {receiver: '^SSB'}},
    { name: 'armin_wedele', :sig => {receiver: 'ARMIN WEDELE'}},
    { name: 'anja_meyer', :sig => {receiver: 'ANJA MEYER'}},
    { name: 'qigong_fortbildung', :sig => {receiver: 'Yi Zhou'}},
    { name: 'qigong_fortbildung_2', :sig => {comment: 'qigong'}},
    { name: 'tiefenimagination', :sig => {receiver: 'greiner|seminarhaus holzmann', comment: 'seminar|training|tiefen|traum|reise'}},
    { name: 'tiefenimagination_2', :sig => {comment: 'tiefenimagination|traumreise'}},
    { name: 'tiefenimagination_3', :sig => {receiver: 'deep inner space'}},
    { name: 'sanitär_auch', :sig => {receiver: 'Sanitär Auch'}},
    { name: 'lüftung', :sig => {receiver: 'PLUGGIT'}},
    { name: 'ausstatter_div', :sig => {receiver: 'Plissee-Held'}}, # Plissee-Held,
    { name: 'spielwaren_div', :sig => {receiver: 'COCOO NINO|HEPROS GMBH'}}, # Cocoo Nino im Breuningerland
    { name: 'läden_div', :sig => {receiver: 'koecheler|Elektro-Elsässer|Tchibo GmbH'}}, # Geschenklädle Waldenbuch
    { name: 'klavier_miete', :sig => {receiver: 'BRIEM KLAVIERE|Klavier Briem'}},
    { name: 'rechtsanwälte', :sig => {receiver: 'Jost Richter'}},
    { name: 'friseur', :sig => {receiver: 'AMAL.*FRISEUR|VITHA HAIR'}},
    { name: 'stadt_waldenbuch', :sig => {receiver: 'STADT WALDENBUCH$'}},
    { name: 'drf', :sig => {receiver: 'DRF e.V.'}},
    { name: 'camping', :sig => {comment: 'camping\W+|\\/[ES|FR]+\\/'}},
    { name: 'reisen_diverse', :sig => {receiver: 'hellener'}}
  ]

  RULES = [
    {:name => 'Nobody', :targets => [:nobody]}, # the fallback rule should only match the fallback posting-entry (see above)
    {:name => 'Arbeit:Einkommen', :targets => [:msc_gehalt, :ittem_gehalt, :activpersonal_gehalt]},
    # TODO: Remove "einkommen_via_deutsche_bank_konto"-entry once we get this information directly from Deutsche-Bank:
    {:name => 'Arbeit:Einkommen:Selbständig', :targets => [:einkommen_diebold, :einkommen_fischer_consulting, :einkommen_forum_der_kulturen, :einkommen_xitcom, :einkommen_via_deutsche_bank_konto, :einkommen_via_deutsche_bank_konto_2]},
    {:name => 'Arbeit:Bellicon', :targets => [:bellicon_deutschland, :bellicon_div]},
    {:name => 'Schenkung:Eltern', :targets => [:eltern]},
    {:name => 'Kindergeld',    :targets => [:kindergeld]},
    {:name => 'Elterngeld',    :targets => [:elterngeld]},
    {:name => 'Haus', :targets => [:armin_wedele, :anja_meyer, :lüftung, :sanitär_auch, :ausstatter_div]},
    {:name => 'Rechtsanwälte', :targets => [:rechtsanwälte]},
    {:name => 'Auto:Steuern', :targets => [:auto_steuern]},
    {:name => 'Auto:Versicherung', :targets => [:auto_versicherung_1, :auto_versicherung_2]},
    {:name => 'Auto:Benzin', :targets => [:tankstellen, :tankstellen_2]},
    {:name => 'Auto:Carsharing', :targets => [:carsharing_seligs, :stadtmobil]},
    {:name => 'Auto:Werkstatt', :targets => [:auto_werkstatt_arnold, :auto_werkstatt_mueller, :auto_werkstatt_jaeger, :auto_teile_div]},
    {:name => 'Auto:ADAC', :targets => [:adac]},
    {:name => 'Kredite',       :targets => [:tilgung_kredit_var, :tilgung_kredit_var_2]},
    {:name => 'Kredite:Haus',       :targets => [:tilgung_kredit_haus, :tilgung_kredit_haus_commerzbank]},
    {:name => 'Kredite:Haus:Sondertilgungen', :targets => [:sondertilgung_kredit_haus, :sondertilgung_kredit_haus_1]},
    {:name => 'Nebenkosten:Strom', :targets => [:strom_enbw_1, :strom_enbw_2, :strom_stadtwerke_tue_1, :strom_stadtwerke_tue_2, :strom_stadtwerke_tue_3, :strom_knauber, :strom_stadtwerke_oranienburg]},
    {:name => 'Nebenkosten:Wasser', :targets => [:wasser]},
    {:name => 'Nebenkosten:Abfall', :targets => [:abfall_betrieb_bb, :abfall_betrieb_bb_2]},
    {:name => 'Nebenkosten:Schornsteinfeger', :targets => [:schornsteinfeger_beetz]},
    {:name => 'Nebenkosten:Heizung', :targets => [:wartung_heizung, :wartung_heizung_lindheimer]},
    {:name => 'Vereine', :targets => [:verein_flötentöne_zirkus, :verein_tsv_waldebuch, :verein_tsv_leinfelden, :verein_chorverein_waldenbuch, :verein_ibm_club]},
    {:name => 'Vereine:EWTO', :targets => [:verein_ewto]},
    {:name => 'Waki', :targets => [:waki]},
    {:name => 'Faulhaber:DL1', :targets => [:faulhaber_dl_v1, :faulhaber_dl_v2, :faulhaber_dl_v3]},
    {:name => 'Handwerker', :targets => [:faupel, :handwerker_1]},
    {:name => 'Medien:GEZ', :targets => [:gez_1, :gez_2, :gez_3]},
    {:name => 'Medien:Internet', :targets => [:kabelbw_1, :kabelbw_2, :telekom]},
    {:name => 'Medien:Handy', :targets => [:o2_handy1, :congstar_handy, :eins_und_eins_handy, :klarmobil_handy, :drillisch_handy, :aldi_talk_handy]},
    {:name => 'Medien:Hoster', :targets => [:internet_hoster]},
    {:name => 'Medien:Zeitungen', :targets => [:nzz, :zeitungen_div]},
    {:name => 'Medien:Zeitschriften:MKW', :targets => [:mit_kindern_wachsen_1, :mit_kindern_wachsen_2]},
    {:name => 'Medien:Zeitschriften:ct', :targets => [:ct_magazin]},
    {:name => 'Medien:Zeitschriften:unerzogen', :targets => [:unerzogen]},
    {:name => 'Medien:Zeitschriften:LTB', :targets => [:disney_ltb]},
    {:name => 'Medien:Zeitschriften:GEO', :targets => [:magazin_geo_1, :magazin_geo_2]},
    {:name => 'Schule:Fas:Schulgeld', :targets => [:fas_schulgeld]},
    {:name => 'Schule:Fas:Spenden', :targets => [:fas_spenden]},
    {:name => 'Schule:Fas', :targets => [:fas_rest, :fas_ausflüge, :fas_ausflüge_2, :fas_schliessfächer]},
    {:name => 'Schule:Fas:Putzen', :targets => [:fas_putzen_ehlers, :fas_putzen_div]},
    {:name => 'Unterhalt:Lebensmittel', :targets => [:lebensmittel_läden, :lebensmittel_1, :lebensmittel_2, :lebensmittel_3, :lebensmittel_4, :lebensmittel_5, :lebensmittel_div]},
    {:name => 'Unterhalt:Lebensmittel:Restaurants', :targets => [:restaurants_1, :restaurants_2]},
    {:name => 'Unterhalt:Drogerie', :targets => [:drogerie_dm_1, :drogerie_dm_2, :drogerie_schlecker]},
    {:name => 'Unterhalt:Paypal', :targets => [:paypal_1, :paypal_2]},
    {:name => 'Unterhalt:Amazon', :targets => [:amazon]},
    {:name => 'Unterhalt:Verkehr', :targets => [:ssb_1, :ssb_2, :bahn_1]}, # ÖPV, S-Bahn, Bus, ...
    {:name => 'Unterhalt:Kleidung', :targets => [:kleidung_diverse, :schuhhaus_bubser, :schuhhäuser]},
    {:name => 'Unterhalt:Möbel', :targets => [:möbel_div]},
    {:name => 'Unterhalt:Bücher', :targets => [:bücher_1, :bücher_hörspiele]},
    {:name => 'Unterhalt:Baumärkte', :targets => [:baumärkte]},
    {:name => 'Unterhalt:Klavier', :targets => [:klavier_miete]},
    {:name => 'Unterhalt:Unterhaltung', :targets => [:unterhaltung_elektro_div]},
    {:name => 'Unterhalt:Freizeit', :targets => [:spiel_und_spass_div, :fahrrad_div]},
    {:name => 'Unterhalt:Freizeit:Tiefenimagination', :targets => [:tiefenimagination, :tiefenimagination_2, :tiefenimagination_3]},
    {:name => 'Unterhalt:Freizeit:Qigong', :targets => [:qigong_fortbildung, :qigong_fortbildung_2]},
    {:name => 'Unterhalt:Freizeit:Skischullandheim', :targets => [:ski_schullandheim, :ski_schullandheim_ausrüstung]},
    {:name => 'Unterhalt:Hund', :targets => [:hunde_pflege, :hunde_futter, :hunde_futter_2, :hunde_steuer, :hunde_haftpflicht_nv, :hunde_tierarzt]},
    {:name => 'Unterhalt:Kinder:Taschengeld', :targets => [:nicole_taschengeld_voba, :nicole_taschengeld_voba_2, :nicole_taschengeld_voba_3]},
    {:name => 'Unterhalt:Kinder:Kurse', :targets => [:emma_kurs_gitarre]},
    {:name => 'Unterhalt:Gebühren', :targets => [:stadt_waldenbuch]},
    {:name => 'Unterhalt:Diverses', :targets => [:kaufhäuser, :spielwaren_div, :gaertnereien, :nanu_nana, :friseur, :läden_div]},
    {:name => 'Giro:AndereKonten', :targets => [:andere_voba_konten]},
    {:name => 'Giro:Kosten', :targets => [:konto_voba, :onlinebanking_voba]},
    {:name => 'Giro:LBBW', :targets => [:lbbw_konto]},
    {:name => 'Giro:Barauszahlungen', :targets => [:ec_automat_1, :ec_automat_2, :ec_automat_3, :ec_automat_4, :ec_automat_5, :ec_automat_voba_fellbach, :barauszahlung_giro]},
    {:name => 'Giro:Kreditkarten', :targets => [:kreditkarte_lufthansa_1, :kreditkarte_lufthansa_2, :kreditkarte_visa_voba, :kreditkarte_barclaycard_platinum]},
    {:name => 'Giro:Sparkonten:Nicole', :targets => [:sparkonto_nicole_voba]},
    {:name => 'Giro:Sparkonten:Daniel', :targets => [:sparkonto_daniel_voba]},
    {:name => 'Giro:Sparkonten:Emma', :targets => [:sparkonto_emma_voba]},
    {:name => 'Finanzen:N26:Ling', :targets => [:n26_konto_ling]},
    {:name => 'Finanzen:Depotkosten', :targets => [:depotkosten_deutsche_bank]},
    {:name => 'Versicherungen:Haftpflicht:Claus', :targets => [:debeka_haftpflicht_claus_1, :debeka_haftpflicht_claus_2]},
    {:name => 'Versicherungen:Rente:Ling', :targets => [:debeka_rente_ling_1, :debeka_rente_ling_2, :neue_bayrische_leben]},
    {:name => 'Versicherungen:Rente:Claus', :targets => [:r_und_v_versicherung, :aachen_muenchener_claus_basis_und_riester_rente, :generali_claus_basis_und_riester_rente]},
    {:name => 'Versicherungen:Kranken:Kinder', :targets => [:union_krankenversicherung_kinder]},
    {:name => 'Versicherungen:Kranken:Kinder:Erstattungen', :targets => [:union_krankenversicherung_kinder_erstattungen]},
    {:name => 'Versicherungen:Kranken:Ling', :targets => [:hansemerkur_vers_beiträge_1, :hansemerkur_vers_beiträge_2]},
    {:name => 'Versicherungen:Berufsunfähigkeits', :targets => [:sparkassen_vers_beiträge_1, :sparkassen_vers_beiträge_2]},
    {:name => 'Versicherungen:Gebäude', :targets => [:dr_muente]},
    {:name => 'Versicherungen:Reisen', :targets => [:reiseversicherungen]},
    {:name => 'Versicherungen:Krankenversicherung:Claus', :targets => [:techniker_krankenkasse_vers_beiträge]},
    {:name => 'Reisen', :targets => [:yu_mei_reisebüro, :div_reisebüros, :camping, :reisen_diverse]},
    {:name => 'Steuern:Umsatz', :targets => [:finanzamt_stuttgart_umsatzsteuer]},
    {:name => 'Steuern', :targets => [:finanzamt_stuttgart]},
    {:name => 'Steuern:Beratung', :targets => [:steuersoftware_buhldata]},
    {:name => 'Steuern:Grund', :targets => [:waldenbuch_grundsteuer]},
    {:name => 'Gesundheit:Ärzte', :targets => [:ärzte, :ärzte_2, :tcm_ärzte]},
    {:name => 'Gesundheit:Ärzte:Zahnärzte', :targets => [:zahnärzte, :zahnärzte_2]},
    {:name => 'Gesundheit:Ärzte:Kieferorthopäden', :targets => [:kieferorthopäden_lastschriften, :kieferorthopäden_lastschriften_2]},
    {:name => 'Gesundheit:Apotheken', :targets => [:apotheken, :apotheken_2]},
    {:name => 'Gesundheit:Brillen', :targets => [:optiker]},
    {:name => 'Spenden:DRF', :targets => [:drf]},
  ]

  def self.fallback_posting
    return Proc.new {POSTINGS.first}
  end

  def self.find_booking_category_for(booking_data)
    POSTINGS.detect(fallback_posting) do |pst|
      # puts "--------------------> #{booking_data.inspect} <-> #{pst.inspect}"
      pst.assoc(:sig)[1].all? do |k, v|
        booking_data[k].match(/#{v}/i) unless booking_data[k].nil?
      end
    end
  end

  # Adds a rule to a posting, if one matches
  def self.apply_rules(posting)
    rule = RULES.detect {|r| r[:targets].any? {|target| target.to_s == posting[:name]}}
    # puts("found target #{rule[:name]} for posting #{posting}") unless rule[:name].blank?
    posting[:rule] = rule unless rule[:name].blank?
  end

  # Given a posting with a rule, convert it to a booking-line with from- and to- accounts
  def self.build_booking_line(post)
    comment = post[:comment]
    post[:desc] = comment ? "#{post[:to]}: #{comment}".strip : post[:to] # NOTE: original post[:to] is overwritten below!

    if post[:type] == 'expense'
      post[:to] = {:name => 'Assets:Checking', :amount => -post[:value]}
      post[:from] = {:name => "Expenses:#{post[:src][:rule][:name]}", :amount => post[:value]}
    else # income
      post[:to] = {:name => "Income:#{post[:src][:rule][:name]}", :amount => -post[:value]}
      post[:from] = {:name => 'Assets:Checking', :amount => post[:value]}
    end
    post
  end

end

class RecurringTransactionGenerator
  def self.generateMonthlyTransaction(tx, dateRange)
    dateFmt = '%Y/%m/%d'

    (dateRange)
      .select {|d| d.day == 1}
      .reduce('') {|txStr, d| txStr + "#{d.strftime(dateFmt)} * expense" + tx + "\n"}
  end
end

class LedgerEmitter
  # for documentation on Ledger see http://cloud.github.com/downloads/jwiegley/ledger/ledger.pdf

  OPENING_BALANCES = %{
2009/02/26 Opening Balance
  Assets:Checking    19188.74
  Equity:Opening Balances
}

  PERIOD_TRANSACTIONS = %{
; Kantine und externe Verpflegung @Fellbach während Projekt iParts
~ every day from 2015/09/14 to 2018/03/31
  Expenses:Unterhalt:Lebensmittel  10.0
  Expenses:Giro:Barauszahlungen
}

  MONTHLY_RECURRING = [
    %{
\tExpenses:Kredite:Haus 		1730.13
\tAssets:Checking 		-1730.13
}
  ]
                        .reduce("\n; BEGIN OF MONTHLY RECURRING TRANSACTIONS\n") {
    |memo, tx| memo << RecurringTransactionGenerator.generateMonthlyTransaction(tx, Date.new(2008, 01)..Date.new(2009, 02))
  }
                        .concat('; END OF MONTHLY RECURRING TRANSACTIONS')

  def initialize(postings)
    # Prepend a static header with the last total seen on checking-account when
    # our accounting-history starts:
    @header = OPENING_BALANCES + PERIOD_TRANSACTIONS + MONTHLY_RECURRING
    @postings = postings
  end

  def emit
    File.open("ledger.dat", 'w+') do |f|
      f.puts '; -*- ledger -*-';
      f.puts @header; f.puts
      @postings.each {|post|
        f << "#{post[:when]} * #{post[:type]}: #{post[:comment]} ; #{post[:desc]}\n"
        f << "\t#{post[:from][:name]} \t\t#{post[:from][:amount]}\n"
        f << "\t#{post[:to][:name]} \t\t#{post[:to][:amount]}\n"
        f.puts
      }
    end
    puts "Done emitting: #{@postings.size} posts."
  end

end

AccountDataCSVImporter.run
