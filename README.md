## nostrum

This is a collection of shell-scripts and some Ruby-code to convert my monthly online-banking statements into a [Ledger](http://www.ledger-cli.org/)-compatible text-file suitable to use for commandline-accounting.

This makes it easy to analyse my financial status using the powers of Ledger.

### Usage

1. Download your banking statement(s) as CSV

2. Adjust the path to your statements in the ./go_voba.sh script and run it. This preformats and joins all banking-csv-files for further processing.

3. run ‘bundle exec ruby nostrum.rb‘ to convert the resulting file into Ledger-format.

4. Run ledger-cli against the ‘ledger.dat’ file.
   see https://devhints.io/ledger for possible cli-syntax and commands
