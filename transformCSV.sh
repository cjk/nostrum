#!/usr/bin/zsh

# Prepare CSV-exports from VoBa online banking and make it them more digestable
# to our csv-to-ledger converter.
#
# v2.0 CjK 05.08.2015

# Create destination filename by appending 'transformed' to the source file's
# basename:
DEST_FILE="voba.csv"

# Delete first 13 lines, those containing useless information and empty lines
# NOTE (CjK 08.11.2021): By prefixing our file-mask with 'Umsaetze*' we avoid picking up CSV-files in other formats!
sed -s -r -e '{1,13d}' -e '{/(Anfangssaldo)|(Endsaldo)/d}' -e '{/^[[:space:]]$/d}' ~/family/banking/Umsaetze*.csv | \

iconv -f ISO8859-1 -t UTF-8                                                                               | \

# Parse CSV-patterns and remove extra line-breaks (lot's of them!) Also convert
# old 10-field output to the new 13-field format, which includes new IBAN, BIC
# and customer-reference fields.
# Thus, old 10-field records are padded to match the new format:
gawk 'BEGIN { RS = "\r\n"; FS = ";"; } {gsub (/\n/," "); printf "%s;%s;%s;%s;%s;", $1, $2, $3, $4, $5; \
      format = "%s;%s;%s;%s;%s;%s;%s;%s"; \
      if (NF > 10)  printf(format,$6,$7,$8,$9,$10,$11,$12,$13); \
      if (NF == 10) printf(format,$6,"","",$7,"",$8,$9,$10); \
  print "";}'                                                                                             | \

sort --unique --field-separator=';' --key=1.8,1.11 --key=1.5,1.6 --key=1.2,1.3 --key=1.27 > $DEST_FILE
# tee $DEST_FILE

echo "Collected + wrote all CSV-data to <$DEST_FILE> - done!"

time ~/.gem/ruby/2.0.0/bin/bundle exec ruby nostrum.rb
