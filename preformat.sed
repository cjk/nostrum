#!/bin/sed -f

## Sanitize + convert a VOBA-text-export into proper CSV-format
## Input file should have been stripped of unnecessary lines already
##
## 08.04.2011 Claus Klingberg <cjk@pobox.com>

# Append this data line to the hold space.
H
# If this is not the last line, continue to the next line.
$!d
# Otherwise, this is the end of the file or the start of a header.
: end
# Call up the data lines we last saw (putting the empty line in the hold).
x
# If we haven't seen any data lines recently, continue to the next line.
/^$/d
# Otherwise replace DOS-linebreaks with a |-marker ...
s/\r\n/\|/g
# ...replace remaining UNIX-linebreaks with spaces (joins the lines in effect)
s/\n/ /g
# ... and then convert the |-markers with a UNIX-LF
s/\|/\n/g
# finally trim leading spaces and print.
s/^ //
